<?xml version='1.0' encoding='utf-8' ?>
<!DOCTYPE preface [

<!ENTITY % entities SYSTEM "entities.ent">
%entities;
]>
<preface xml:id="preface" xmlns="http://docbook.org/ns/docbook" version="5.0"
  xmlns:xlink="http://www.w3.org/1999/xlink">
  <info>
    <title>
      Preface
    </title>

    <keywordset>
      <keyword>software features</keyword> <keyword>intended audience</keyword>
      <keyword>report feedback</keyword>
    </keywordset>
  </info>

  <sect1>

    <title>
      Software feature offerings and intended audience
    </title>

    <para>
      This manual is about the &msXps; mass spectrometric software suite, a
      software environment that contains two modules:
    </para>

    <itemizedlist>
      <listitem>
        <para>
          <emphasis>&massXp;</emphasis> module: Allows users to define brand new
          polymer chemistries and use these polymer chemistry definitions to
          model linear polymer sequence. Once modelled, a polymer sequence can
          undergo chemical reactions (enzymatic or chemical cleavages, gas-phase
          fragmentations&hellip;). The obtained results are a model of what a
          mass spectrum would look like if the modelled experiment had actually
          been carried over up to the mass spectrometry analysis;
        </para>
      </listitem>

      <listitem>
        <para>
          <emphasis>&mineXp;</emphasis> module: Allows users to load mass
          spectrometry data from mzML files, visualize and mine the data
          throughout all their depth. The mass data visualization starts at the
          TIC chromatogram level and deepens to the mass spectra, the drift
          spectra, the XIC chromatograms.
        </para>
      </listitem>
    </itemizedlist>

    <para>
      As such, this manual is intended for people willing to learn how to use
      the comprehensive &msXps; software package.
    </para>

    <para>
      Mass spectrometry has gained popularity across the past twenty years or
      so. Indeed, developments in polymer mass spectrometry have made this
      technique appropriate to accurately measure masses of polymers as heavy as
      many hundreds of kDa, and of any chemical type.
    </para>

    <para>
      There are a number of utilities&mdash;sold by mass spectrometer
      constructors with their machines, usually as a marketing
      <quote>plus</quote>&mdash;that allow predicting/analyzing mass
      spectrometric data obtained on polymers. These programs are usually
      different from a constructor to another. Also, there are as many mass
      spectrometric data prediction/analysis computer programs as there are
      different polymer types. You will get a program for oligonucleotides,
      another one for proteins, maybe there is one program for saccharides, and
      so on. Thus, the biochemist/massist, for example, who happens to work on
      different biopolymer types will have to learn to use several different
      software packages. Also, if the software user does not own a mass
      spectrometer, chances are he will need to buy all these software packages.
    </para>

    <para>
      The &msXps; mass spectrometric software is designed to provide
      <emphasis>free</emphasis> solutions to all these problems by providing the
      following features:
    </para>

    <itemizedlist>
      <listitem>
        <para>
          &massXp;:
        </para>

        <itemizedlist>
          <listitem>
            <para>
              Model
              <foreignphrase>
                ex nihilo
              </foreignphrase>
              polymer chemistry definitions (in the &xpd; module that is part of
              the &massXp; program);
            </para>
          </listitem>

          <listitem>
            <para>
              Peform simple yet powerful mass computations to be made in a mass
              desktop calculator that is both polymer chemistry definition-aware
              and fully programmable (that's the &xpc; module also part of the
              &massXp; program);
            </para>
          </listitem>

          <listitem>
            <para>
              Edit polymer sequences on a polymer chemistry definition-specific
              basis, along with chemical reaction simulations, finely configured
              mass spectrometric computations&hellip; (all taking place in the
              &xpe; module that is the main module of the &massXp; program);
            </para>
          </listitem>

          <listitem>
            <para>
              Customize the way each monomer will show up graphically during the
              program operation (in the &xpe; module);
            </para>
          </listitem>

          <listitem>
            <para>
              Edit polymer sequences with immediate visualization of the mass
              changes elicited by the editing activity (in the &xpe; module);
            </para>
          </listitem>

          <listitem>
            <para>
              Open an unlimited number of polymer sequences at any given time
              and of any given polymer chemistry definition type (in the &xpe;
              module);
            </para>
          </listitem>
        </itemizedlist>
      </listitem>

      <listitem>
        <para>
          &mineXp;:
        </para>

        <itemizedlist>
          <listitem>
            <para>
              Load mass spectometry data files in the mzML format, thanks to the
              excellent
              <application>
                libpwiz
              </application>
              library of ProteoWizard
              <footnote>
                <para>
                  <link
										xlink:href="http://proteowizard.sourceforge.net/"/>
                  .
                </para>
              </footnote>
              fame;
            </para>
          </listitem>

          <listitem>
            <para>
              Display the data in powerful ways in a unified graphical user
              interface. The interface was designed to integrate all the most
              useful characteristics of the various proprietary environments
              known by the author, thanks to the excellent
              <application>
                libqcustomplot
              </application>

              <footnote>
                <para>
                  <link
										xlink:href="http://qcustomplot.com/"/>
                  .
                </para>
              </footnote>
              library;
            </para>
          </listitem>

          <listitem>
            <para>
              Configure the way mass spectrometry data integrations are
              performed and optionally configure and apply a Savitzky-Golay
              smoothing;
            </para>
          </listitem>

          <listitem>
            <para>
              Perform data mining by performing data integrations in various
              ways;
            </para>
          </listitem>

          <listitem>
            <para>
              Ion mobility mass spectrometry data are supported with an
              automatic &mz;&nbsp;=&nbsp;f(&dt;) color map plot calculation;
            </para>
          </listitem>

          <listitem>
            <para>
              A specific data integration mode allows easy quantitation of
              spectral data at any level (TIC chromatogram, mass spectrum, drift
              spectrum);
            </para>
          </listitem>

          <listitem>
            <para>
              Innovative data analysis recording allows to store the features
              mined during the data mining sessions in flexible ways that allow
              further data processing, like injection in databases;
            </para>
          </listitem>

          <listitem>
            <para>
              A <productname>JavaScript</productname> scripting environment
              allows taking the control of the software and of all the widgets
              from a script file;
            </para>
          </listitem>

          <listitem>
            <para>
              Convert data from mzML to the private (albeit open) database file
              format that allows to load data much faster. &mineXp; can slice
              big data files into smaller chunks retaining all the data selected
              by the user in the most flexible ways.
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>

  </sect1>

  <sect1 xml:id="sec_preface-feedback-from-users">

    <title>
      Feedback from the users
    </title>

    <para>
      We are always grateful to any constructive feedback from the users.
    </para>

    <para>
      The msXpertSuite software team might be contacted
      <foreignphrase>
        via
      </foreignphrase>
      the following addresses
    </para>

    <figure xml:id="fig_feedback-addresses">

      <title>
        Addresses to report feedback to
      </title>



      <mediaobject>
        <imageobject role="fo">
          <imagedata fileref="print-feedback-addresses.png" format="PNG" scale="100"/>
        </imageobject>

        <imageobject role="html">
          <imagedata fileref="web-feedback-addresses.png" format="PNG" scale="100"/>
        </imageobject>
      </mediaobject>

    </figure>

  </sect1>

  <sect1>

    <title>
      Project History
    </title>

    <para>
      This is a brief history of &msXps;.
    </para>

    <itemizedlist>
      <listitem>
        <para>
          <emphasis>1998&ndash;2000</emphasis>
        </para>

        <para>
          The name &massXp; comes from a project I started while I was a
          post-doctoral fellow at the &Eacute;cole Polytechnique (Institut
          Européen de Chimie et Biologie, Université&nbsp;Bordeaux&nbsp;1,
          Pessac, France);
        </para>

        <para>
          The &massXp; program was published in
          <emphasis>Bioinformatics</emphasis> (
          <link
						xlink:href="https://academic.oup.com/bioinformatics/article/18/4/644/243311">
            Rusconi, F. and Belghazi, M. <emphasis>Desktop prediction/analysis
            of mass spectrometric data in proteomic projects by using
            massXpert</emphasis>. <emphasis>Bioinformatics</emphasis>, 2002,
            644&ndash;655
          </link>
          ).
        </para>

        <para>
          At that time, <productname>MS-Windows</productname> was at the
          <productname>Windows NT&nbsp;4.0</productname> version and the next
          big release was going to be <emphasis><quote>you'll see what you'll
          see</quote></emphasis>: <productname>MS-Windows 2000</productname>.
        </para>

        <para>
          When I tried &massXp; on that new version (one colleague had it with a
          new machine), I discovered that my software would not run normally
          (the editor was broken). The Microsoft technical staff would advise to
          <emphasis><quote>buy a new version of the compiler environment and
          rebuild</quote></emphasis>. This was a no-go: I did not want to
          continue paying for using something I had already produced with
          legitimate software.
        </para>
      </listitem>

      <listitem>
        <para>
          <emphasis>2001&ndash;2006</emphasis>
        </para>

        <para>
          During fall 1999, I decided that I would stop using
          <productname>Microsoft</productname> products for my development. At
          the beginning of 2000 I started as a CNRS research staff in a new
          laboratory and decided to start fresh: I switched to
          <productname>GNU/Linux</productname> (I never looked back). After some
          months of learning, I felt mature to start a new development project
          that would eventually become an official GNU package: <productname>GNU
          polyxmass</productname>.
        </para>

        <para>
          The <productname>GNU polyxmass</productname> software, much more
          powerful than what the initial &massXp; software used to be, was
          published in <emphasis>BMC Bioinformatics</emphasis> in 2006 (
          <link
						xlink:href="https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-7-226">
            Rusconi, F. <emphasis>GNU polyxmass: a software framework for mass
            spectrometric simulations of linear (bio-)polymeric
            analytes</emphasis>. <emphasis>BMC Bioinformatics</emphasis>,
            225&ndash;
          </link>
          ).
        </para>

        <para>
          Following that publication I got a lot of feedback (very positive, in
          a way) along the lines: &mdash;<emphasis><quote>Hey, your software
          looks very interesting; only it's a pity we cannot use it because it
          runs on GNU/Linux, and we only use
          <productname>MS-Windows</productname> and
          <productname>MacOSX!</productname></quote></emphasis>.
        </para>
      </listitem>

      <listitem>
        <para>
          <emphasis>2007&ndash;2016</emphasis>
        </para>

        <para>
          In december&nbsp;2006, I decided to make a full rewrite of GNU
          polyxmass. The software of which you are reading the user manual is
          the result of that rewrite. I decided to <quote>recycle</quote> the
          &massXp; name because this software is written in C++, as was the
          first &massXp; software. Also, because the first
          <productname>MS-Windows</productname>-based &massXp; project is not
          developped anymore, taking that name was kind of a
          <quote>revival</quote> which I enjoyed. However, the toolkit I used
          this time is not the Microsoft Foundation Classes (first &massXp;
          version) but the Trolltech <productname>Qt</productname> framework
          (see the
          <guimenuitem>
            About Qt
          </guimenuitem>
          menu in the <guimenu>Help</guimenu> menu in &massXp;).
        </para>

        <para>
          Coding with the <productname>Qt</productname> libraries has one big
          advantage: it allows the developer to code once and to compile on the
          three main platforms available today:
          <productname>GNU/Linux</productname>,
          <productname>MacOSX</productname>,
          <productname>MS-Windows</productname>. Another advantage is that the
          <productname>Qt</productname> libraries are wonderful software,
          technically and philosophically (Free Software).
        </para>

        <para>
          The rewritten software was published in 2009 (
          <link
						xlink:href="https://academic.oup.com/bioinformatics/article/25/20/2741/194220">
            Rusconi, F. <emphasis>massXpert 2: a cross-platform software
            environment for polymer chemistry modelling and simulation/analysis
            of mass spectrometric
            data</emphasis>.<emphasis>Bioinformatics</emphasis>, 2009,
            2741&ndash;2742
          </link>
          ).
        </para>
      </listitem>

      <listitem>
        <para>
          <emphasis>2016&ndash;</emphasis>
        </para>

        <para>
          In 2016, I started a new project about visualization of mass
          spectrometric data. The project developed pretty quickly, as we needed
          at the mass spectrometry facility a software that would allow to cope
          efficiently with ion mobility mass spectrometric experimental data.
          &mineXp; was thus started.
        </para>

        <para>
          To bundle both &massXp; and &mineXp; in a single software suite, I
          bought the &msXps; website
          <link xlink:href="http://msxpertsuite.org"/>
          and created that new name.
        </para>
      </listitem>
    </itemizedlist>

  </sect1>

  <sect1>

    <title>
      Program and Documentation Availability and License
    </title>

    <para>
      The programs and all the documentation that are shipped along with the
      &msXps; software suite are available at
      <link
				xlink:href="http://www.msxpertsuite.org"/>
      . Most of the time, a new version is published as source, as binary
      install packages for <productname>MacOSX</productname> and
      <productname>MS-Windows</productname>. No
      <productname>GNU/Linux</productname> are created outside of the autbuilder
      of the various distributions. As a Debian Developer, Filippo Rusconi
      creates <productname>Debian</productname>
      <footnote>
        <para>
          <link
						xlink:href="http://www.debian.org/"/>
        </para>
      </footnote>
      packages that are uploaded on the distribution servers. These packages are
      available using the system's software management infrastructure (like
      <application>
        apt
      </application>
      , for example).
    </para>

    <para>
      The software and all the documentation are all provided under the Free
      Software license <emphasis>GNU General Public License, Version&nbsp;3, or
      later, at your option</emphasis>. For an in-depth study of the
      <emphasis>Free Software</emphasis> philosphy I kindly urge the reader to
      visit
      <link xlink:href="http://www.gnu.org/philosophy"/>
      .
    </para>

  </sect1>
</preface>
