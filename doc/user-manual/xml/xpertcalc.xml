<?xml version='1.0' encoding='utf-8' ?>
<!DOCTYPE chapter [

<!ENTITY % entities SYSTEM "entities.ent">
%entities;
]>
<chapter
	xml:id="chap_xpertcalc-module" 
	xmlns="http://docbook.org/ns/docbook" version="5.0"
	xmlns:xlink="http://www.w3.org/1999/xlink">

  <info>
    <title>
      XpertCalc: a Powerful Mass Calculator
    </title>

    <keywordset>
      <keyword>chemical formula</keyword> <keyword>action-formula</keyword>
      <keyword>calculator</keyword>
    </keywordset>
  </info>

  <para>
    After having completed this chapter you will be able to perform
    sophisticated polymer chemistry-aware mass calculations.
  </para>

  <sect1 xml:id="sec_xpertcalc-invocation">

    <title>
      XpertCalc Invocation
    </title>

    <para>
      The &xpc; module is easily called by pulling down the
      <guimenuitem>
        XpertCalc
      </guimenuitem>
      menu item from the &massXp; program's menu. The user is presented with a
      window to select the polymer chemistry definition that should be used for
      the calculations (
      <xref
				linkend="fig_xpertcalc-select-pol-chem-def"/>
      ).
    </para>

    <figure xml:id="fig_xpertcalc-select-pol-chem-def">

      <title>
        Selecting a polymer chemistry definition for use with XpertCalc
      </title>



      <mediaobject>
        <imageobject role="fo">
          <imagedata fileref="print-xpertcalc-select-pol-chem-def.png" format="PNG" scale="100"/>
        </imageobject>

        <imageobject role="html">
          <imagedata fileref="web-xpertcalc-select-pol-chem-def.png" format="PNG" scale="80"/>
        </imageobject>

        <caption>
          <para>
            This figure shows that the user can either select one already
            registered polymer chemistry definition (listed in the drop-down
            widget) or browse the filesystem to select one polymer chemistry
            definition file. Choosing a polymer chemistry definition allows to
            take advantage, during the mass calculations, of all the chemical
            entities defined therein.
          </para>
        </caption>
      </mediaobject>

    </figure>

  </sect1>

  <sect1 xml:id="sec_xpertcalc-easy-operation">

    <title>
      An Easy Operation
    </title>

    <para>
      Once the polymer chemistry definition has been correctly selected, it is
      parsed by the &xpc; module and its entities are automatically made
      available in the calculator window, as shown in
      <xref
				linkend="fig_xpertcalc-protein-1-letter"/>
      . The way &xpc; is operated is very easy. This is partly due to the very
      self-explanatory graphical user interface of the module, which is
      illustrated in
      <xref
				linkend="fig_xpertcalc-protein-1-letter"/>
      . &xpc; can handle a number of items that are reviewed below:
    </para>

    <itemizedlist>
      <listitem>
        <para>
          The user may (is not obliged to) seed the calculation by setting
          masses manually in the <guilabel>Seed masses</guilabel> line edit
          widgets (the left line edit is for <guilabel>mono</guilabel> and the
          right one for <guilabel>avg</guilabel>);
        </para>

        <warning>
          <para>
            Both monoisotopic and average &mz; values need to be entered.
          </para>
        </warning>

        <para>
          For example, imagine that a mass spectrum analysis session ends up
          like this: &mdash;<emphasis><quote>There is a peak with
          &mz;&nbsp;1000.55, z=1 and another one roughly 80&nbsp;Da more. Is it
          possible that the analyte showing up at &mz;&nbsp;1000.55 is
          phopshorylated?</quote></emphasis>. The mass spectrometrist would seed
          the calculator with mass&nbsp;<guilabel>1000.55</guilabel> and ask
          that one <guilabel>Phosphorylation</guilabel> modification be added to
          it by setting <guilabel>1</guilabel> in front of the corresponding
          drop-down widget. Clicking <guilabel>Apply</guilabel> triggers the
          calculation, with the resulting masses being displayed in the
          <guilabel>Result masses</guilabel> line edit widgets. We can see that
          the phosphorylation of our analyte shifts its &mz; value from
          <guilabel>1000.55</guilabel> to <guilabel>1080.5163</guilabel>.
        </para>

        <tip>
          <para>
            Each time a calculation is triggered by clicking
            <guilabel>Apply</guilabel> (or the chemical pad's buttons; see
            below), the values already present in the <guilabel>Result
            masses</guilabel> line edit widgets are transferred to the
            <guilabel>Seed masses</guilabel> line edit widgets. This provides a
            1-level undo;
          </para>
        </tip>
      </listitem>

      <listitem>
        <para>
          The <guilabel>Formula</guilabel> group box widget contains two
          widgets: a line edit widget where the formula is typed and a count
          spin box widget where the user sets the number of times that the
          formula should be applied. Setting the formula to
          <guilabel>H2O</guilabel> and the count to <guilabel>2</guilabel> would
          hydrate the analyte twice.
        </para>
      </listitem>

      <listitem>
        <para>
          The <guilabel>Polymer Chemistry Definition Entities</guilabel> group
          box widget contains two drop-down widgets and a line edit widget. The
          drop-down widget on the left lists all the monomers defined in the
          <guilabel>protein-1-letter</guilabel> polymer chemistry definition;
          the drop-down widget on the right lists all the modifications defined
          in the <guilabel>protein-1-letter</guilabel> polymer chemistry
          definition. Each drop-down widget has its corresponding count spin box
          widget. In the example, the user asked that one
          (<guilabel>1</guilabel>) <guilabel>Phosphorylation</guilabel>
          modification be applied during the calculation. The line edit widget
          below the first row of widgets is the polymer sequence widget where
          the user might enter a sequence of monomers. It is possible to apply
          many times the sequence by setting the count spin box widget value to
          something greater than 1 (either positive or negative);
        </para>
      </listitem>
    </itemizedlist>

    <para>
      It is possible to perform a set of calculations in one go, that is, the
      user may ask for a formula, a monomer, a modification, a sequence to be
      accounted in one single calculation operation. Once all the chemical
      entities to be taken into account have been set, the user clicks
      <guibutton>
        Apply
      </guibutton>
      : all the entities are parsed in sequence and their mass equivalent are
      added to the result masses. Other prominent features of &xpc; are
      described in the following sections.
    </para>

    <figure xml:id="fig_xpertcalc-protein-1-letter">

      <title>
        Interface of the XpertCalc module
      </title>



      <mediaobject>
        <imageobject role="fo">
          <imagedata fileref="print-xpertcalc-protein-1-letter.png" format="PNG" scale="100"/>
        </imageobject>

        <imageobject role="html">
          <imagedata fileref="web-xpertcalc-protein-1-letter.png" format="PNG" scale="80"/>
        </imageobject>

        <caption>
          <para>
            This figure shows that the &xpc; polymer chemistry definition-aware
            module can handle atoms, formul&aelig;, monomers, modifications and
            even polymer sequences for computing masses.
          </para>
        </caption>
      </mediaobject>

    </figure>

  </sect1>

  <sect1 xml:id="sec_xpertcalc-programmable-calculator">

    <title>
      The Programmable Calculator
    </title>

    <para>
      For the scientists who work on molecules that are often modified in the
      same usual ways, &xpc; features a built-in mechanism by which they can
      easily program their calculator. This programming involves the definition
      of how a <emphasis>chemical pad</emphasis> (or
      <emphasis>chempad</emphasis>) may be arranged, exactly the same way as a
      desktop calculator would display its numerical keypad.
    </para>

    <figure xml:id="fig_xpertcalc-protein-1-letter-chempad">

      <title>
        Interface of the chemical pad
      </title>



      <mediaobject>
        <imageobject role="fo">
          <imagedata fileref="print-xpertcalc-protein-1-letter-chempad.png" format="PNG" scale="100"/>
        </imageobject>

        <imageobject role="html">
          <imagedata fileref="web-xpertcalc-protein-1-letter-chempad.png" format="PNG" scale="80"/>
        </imageobject>

        <caption>
          <para>
            This figure shows that the chemical pad is very similar to what a
            numerical calculator would display. Here, the user has programmed a
            number of chemical reactions.
          </para>
        </caption>
      </mediaobject>

    </figure>

    <para>
      The chemical pad can be shown/hidden by using the <guilabel>Show Chemical
      Pad</guilabel> check box widget. An example of such a chemical pad is
      shown in
      <xref linkend="fig_xpertcalc-protein-1-letter-chempad"/>
      , where a <quote>protein-1-letter</quote> polymer chemistry
      definition-associated chempad is featured. As shown, the user has
      programmed a number of chemical reactions that may be applied to the
      masses in the &xpc; calculator window by simply clicking on their
      respective button (see
      <xref
				linkend="fig_xpertcalc-protein-1-letter-chempad"/>
      ). The configuration of the chemical pad is very easy.
    </para>

    <tip>
      <para>
        It is recommended to copy one of the <filename>chemPad.conf</filename>
        configuration files in any of the polymer chemistry definitions
        distributed within &massXp; and to modify it according to the
        instructions at the top of the file.
      </para>
    </tip>

    <para>
      One example of such a configuration file is shown below along with
      explanations:
    </para>

    <informalexample>
      <screen>
        color%aliceblue%240,248,255
        <co xml:id="co.colordef"/>
        color%antiquewhite%250,235,215 color%aqua%0,255,255 chempad_columns%3
        <co xml:id="co.columns"/>
        chempadgroup%Generic
        <co xml:id="co.genericgroup"/>
        chempadkey=protonate%+H1%adds a proton
        <co xml:id="co.buttondefinition"/>
        chempadkey=hydrate%+H2O1%adds a water molecule
        chempadkey=0H-ylate%+O1H1%adds an hydroxyl group
        chempadkey=acetylate%-H1+C2H3O1%adds an acetyl group
        chempadkey=phosphorylate%-H+H2PO3%add a phosphate group
        chempadkey=sulfide bond%-H2%oxydizes with loss of hydrogen
        chempadgroup%Hexoses &amp; Fucose%[seagreen]
        <co xml:id="co.hexosesandfucosegroup"/>
        chempadkey%Res-1bRE-Hexose%C6H11O6%residue Hexose
        (1bRE)%[lawngreen,black]
        <co xml:id="co.coloredbuttondefinition"/>
        chempadkey%Res-1bRE-Hexalditol%C6H12O6%residue Hexalditol
        (1bRE-ol)%[lawngreen,black]
      </screen>

      <calloutlist>
        <callout arearefs="co.colordef">
          <para>
            It is possible to define as many colors as necessary (red,blue,
            green format, on a scale of 0&ndash;255).
          </para>
        </callout>

        <callout arearefs="co.columns">
          <para>
            The calculator chemical pad shall have its buttons organized in
            three columns.
          </para>
        </callout>

        <callout arearefs="co.genericgroup">
          <para>
            This separator will create a group box widget labelled "Generic"
            that will be populated with all the items found below (until another
            separator is encountered).
          </para>
        </callout>

        <callout arearefs="co.buttondefinition">
          <para>
            A button definition is introduced by the <quote>chempadkey=</quote>
            string. Separated by <quote>%</quote> characters, follow the name of
            the chemical reaction that will label the button
            (<quote>protonate</quote>), the chemical formula of the reaction
            (<quote>+H2O1</quote>) and finally the tooltip text that displays
            when the cursor stays on the button.
          </para>
        </callout>

        <callout arearefs="co.hexosesandfucosegroup">
          <para>
            Separator that starts a new button group box labelled <quote>Hexoses
            &amp; Fucose</quote>. This syntax allows for the coloring of the
            group box widget.
          </para>
        </callout>

        <callout arearefs="co.coloredbuttondefinition">
          <para>
            A button definition that also specifies the coloring of the button.
            <quote>lawngreen</quote> is the background color and
            <quote>black</quote> is the color of the text.
          </para>
        </callout>
      </calloutlist>
    </informalexample>

    <para>
      These buttons might be used in two distinct ways:
    </para>

    <itemizedlist>
      <listitem>
        <para>
          Upon clicking the button, its formula is evaluated and the
          corresponding masses are added to (or subtracted from) the
          <guilabel>Result masses</guilabel>;
        </para>
      </listitem>

      <listitem>
        <para>
          Upon simultaneous clicking the button and keeping the
          <keycap
							function="control"/>
          key pressed, its formula is inserted into the
          <guilabel>Formula</guilabel> line edit widget. In this case, the
          formula is not evaluated and the <guilabel>Result masses</guilabel>
          are not modified.
          <tip>
            <para>
              Clicking sequentially on various chemical pad buttons, append the
              formul&aelig; in the <guilabel>Formula</guilabel> line edit
              widget, which can be useful for storing the whole formula string
              in the memory using the <guilabel>Add formula to memory</guilabel>
              menu item from the drop-down menu before clicking
              <guilabel>Apply</guilabel>.
            </para>
          </tip>
        </para>
      </listitem>
    </itemizedlist>

  </sect1>

  <sect1 xml:id="sec_xpertcalc-logbook-recorder">

    <title>
      The Log Book Recorder
    </title>

    <para>
      Each time an action that is chemically relevant&mdash;from a molecular
      mass point of view&mdash;is performed, the program dumps the calculations
      to the &xpc; recorder window (
      <xref
					linkend="fig_xpertcalc-protein-1-letter-recorder"/>
      ). The recorder can be shown/hidden by using the <guilabel>Show
      Recorder</guilabel> check box widget. The text in the recorder window is
      editable for the user to edit the &xpc; output, and selectable also, so
      that pasting to text editors or word processors is easy
      <emphasis>via</emphasis> the clipboard.
    </para>

    <figure xml:id="fig_xpertcalc-protein-1-letter-recorder">

      <title>
        The XpertCalc recorder window
      </title>



      <mediaobject>
        <imageobject role="fo">
          <imagedata fileref="print-xpertcalc-protein-1-letter-recorder.png" format="PNG" scale="100"/>
        </imageobject>

        <imageobject role="html">
          <imagedata fileref="web-xpertcalc-protein-1-letter-recorder.png" format="PNG" scale="80"/>
        </imageobject>

        <caption>
          <para>
            This figure shows that the recorder window is a simple text edit
            widget that records all the mass-significant operations in the &xpc;
            calculator. The text in the recorder may be selected and later used
            in an electronic logbook or printed.
          </para>
        </caption>
      </mediaobject>

    </figure>

  </sect1>

  <sect1 xml:id="sec_mz-ratio-calculator">

    <title>
      The m/z Ratio Calculator
    </title>

    <para>
      It very often happens that the mass spectrometrist doing electrospray
      analyzes is faced with a challenging task: to compute by mind all the &mz;
      ratios for a given family of charge peaks. To ease that daunting task,
      &xpc; contains a &mz; ratio calculator that is called by clicking onto the
      <guibutton>
        m/z calculation
      </guibutton>
      button.
    </para>

    <para>
      The m/z ratio calculator has been described at&nbsp;
      <xref
					linkend="sec_mz-ratio-calculations"/>
      (see Figure&nbsp;
      <xref
					linkend="fig_xpertedit-mz-ratio-calculator"/>
      .
    </para>

  </sect1>

  <sect1 xml:id="sec_xpertcalc-isotopic-peaks-calculator">

    <title>
      The Isotopic Peaks Calculator
    </title>

    <para>
      This section is no more appropriate for version 6.0.0 and upper as it is
      now implemented in &mineXp;2. Please refer to that software user manual.
    </para>

  </sect1>

</chapter>
