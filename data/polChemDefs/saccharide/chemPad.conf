# This is the layout for the chempad of the molecular calculator 'extremecalc'
# in the massXpert software program.
# Copyright (C) 2002,2003,2004,2005,2006,2007,2008,2009 Filippo Rusconi. 
# GNU General Public License.

# The format of this file allows the definition of colors
# according to the following syntax:
#
# color%OneColor%xxx,yyy,zzz 
#
# with xxx, yyy and zzz numerical values in range [0,255] for the red,
# green and blue component of the color respectively.
#
# Available colors are: white, black, red, darkRed, green, darkGreen,
#  blue, darkBlue, cyan, darkCyan, magenta, darkMagenta, yellow,
#  darkYellow, gray, darkGray, lightGray, please, see
#  http://www.w3.org/TR/SVG/types.html#ColorKeywords for a list of
#  commonly used colors associated to their rgb value.
#
# A separator might be used between sections of buttons. Its syntax is:
# Note the double && that acts as an escape of the &.
#
# chempadsep%Hexoses & Fucose%[midnightblue]
#
# The color specification (%[midnightblue]) is not compulsory

# General button syntax is like the example below:
#
# chempadkey%hydrate%+H2O1%adds a water molecule%[red,blue]
# 
# Each line is divided into 4 compulsory elements:
# 'chempadkey' starts the line
# 'hydrate' is the label of the button
# '+H2O1' is the formula that is applied when the button gets clicked
# 'adds a water molecule' is the text to display in the tooltip
# '[red,blue]' are the background and foreground colors, respectively
#
# The colors specification (last %[color,color]) is not compulsory
#
# The geometry is specified by the number of columns of buttons:
# chempad_columns%3 would specify that all the buttons in the 
# chemical pad should be laid out using three columns and as
# many rows as necessary to display them all.

chempad_columns%3


color%aliceblue%240,248,255
color%antiquewhite%250,235,215
color%aqua%0,255,255
color%aquamarine%127,255,212
color%azure%240,255,255
color%beige%245,245,220
color%bisque%255,228,196
color%black%0,0,0
color%blanchedalmond%255,235,205
color%blue%0,0,255
color%blueviolet%138,43,226
color%brown%165,42,42
color%burlywood%222,184,135
color%cadetblue%95,158,160
color%chartreuse%127,255,0
color%chocolate%210,105,30
color%coral%255,127,80
color%cornflowerblue%100,149,237
color%cornsilk%255,248,220
color%crimson%220,20,60
color%cyan%0,255,255
color%darkblue%0,0,139
color%darkcyan%0,139,139
color%darkgoldenrod%184,134,11
color%darkgray%169,169,169
color%darkgreen%0,100,0
color%darkgrey%169,169,169
color%darkkhaki%189,183,107
color%darkmagenta%139,0,139
color%darkolivegreen%85,107,47
color%darkorange%255,140,0
color%darkorchid%153,50,204
color%darkred%139,0,0
color%darksalmon%233,150,122
color%darkseagreen%143,188,143
color%darkslateblue%72,61,139
color%darkslategray%47,79,79
color%darkslategrey%47,79,79
color%darkturquoise%0,206,209
color%darkviolet%148,0,211
color%deeppink%255,20,147
color%deepskyblue%0,191,255
color%dimgray%105,105,105
color%dimgrey%105,105,105
color%dodgerblue%30,144,255
color%firebrick%178,34,34
color%floralwhite%255,250,240
color%forestgreen%34,139,34
color%fuchsia%255,0,255
color%gainsboro%220,220,220
color%ghostwhite%248,248,255
color%gold%255,215,0
color%goldenrod%218,165,32
color%gray%128,128,128
color%grey%128,128,128
color%green%0,128,0
color%greenyellow%173,255,47
color%honeydew%240,255,240
color%hotpink%255,105,180
color%indianred%205,92,92
color%indigo%75,0,130
color%ivory%255,255,240
color%khaki%240,230,140
color%lavender%230,230,250
color%lavenderblush%255,240,245
color%lawngreen%124,252,0
color%lemonchiffon%255,250,205
color%lightblue%173,216,230
color%lightcoral%240,128,128
color%lightcyan%224,255,255
color%lightgoldenrodyellow%250,250,210
color%lightgray%211,211,211
color%lightgreen%144,238,144
color%lightgrey%211,211,211
color%lightpink%255,182,193
color%lightsalmon%255,160,122
color%lightseagreen%32,178,170
color%lightskyblue%135,206,250
color%lightslategray%119,136,153
color%lightslategrey%119,136,153
color%lightsteelblue%176,196,222
color%lightyellow%255,255,224
color%lime%0,255,0
color%limegreen%50,205,50
color%linen%250,240,230
color%magenta%255,0,255
color%maroon%128,0,0
color%mediumaquamarine%102,205,170
color%mediumblue%0,0,205
color%mediumorchid%186,85,211
color%mediumpurple%147,112,219
color%mediumseagreen%60,179,113
color%mediumslateblue%123,104,238
color%mediumspringgreen%0,250,154
color%mediumturquoise%72,209,204
color%mediumvioletred%199,21,133
color%midnightblue%25,25,112
color%mintcream%245,255,250
color%mistyrose%255,228,225
color%moccasin%255,228,181
color%navajowhite%255,222,173
color%navy%0,0,128
color%oldlace%253,245,230
color%olive%128,128,0
color%olivedrab%107,142,35
color%orange%255,165,0
color%orangered%255,69,0
color%orchid%218,112,214
color%palegoldenrod%238,232,170
color%palegreen%152,251,152
color%paleturquoise%175,238,238
color%palevioletred%219,112,147
color%papayawhip%255,239,213
color%peachpuff%255,218,185
color%peru%205,133,63
color%pink%255,192,203
color%plum%221,160,221
color%powderblue%176,224,230
color%purple%128,0,128
color%red%255,0,0
color%rosybrown%188,143,143
color%royalblue%65,105,225
color%saddlebrown%139,69,19
color%salmon%250,128,114
color%sandybrown%244,164,96
color%seagreen%46,139,87
color%seashell%255,245,238
color%sienna%160,82,45
color%silver%192,192,192
color%skyblue%135,206,235
color%slateblue%106,90,205
color%slategray%112,128,144
color%slategrey%112,128,144
color%snow%255,250,250
color%springgreen%0,255,127
color%steelblue%70,130,180
color%tan%210,180,140
color%teal%0,128,128
color%thistle%216,191,216
color%tomato%255,99,71
color%turquoise%64,224,208
color%violet%238,130,238
color%wheat%245,222,179
color%white%255,255,255
color%whitesmoke%245,245,245
color%yellow%255,255,0
color%yellowgreen%154,205,50


chempadgroup%Generic

chempadkey%protonate%+H1%adds a proton
chempadkey%hydrate%+H2O1%adds a water molecule
chempadkey%0H-ylate%+O1H1%adds an hydroxyl group
chempadkey%acetylate%-H1+C2H3O1%adds an acetyl group
chempadkey%phosphorylate%-H+H2PO3%add a phosphate group
chempadkey%sulfide bond%-H2%oxydizes with loss of hydrogen


chempadgroup%Hexoses && Fucose && Xylose%[orange]

chempadkey%Res-1bRE-Hexose%C6H11O6%residue Hexose (1bRE)%[lawngreen,black]
chempadkey%Res-1bRE-Hexalditol%C6H12O6%residue Hexalditol (1bRE-ol)%[lawngreen,black]
chempadkey%Res-1bNRE-Hexose%C6H11O5%residue Hexose (1bNRE)%[lawngreen,black]
chempadkey%Res-2b-Hexose%C6H10O5%residue Hexose (2b)%[lawngreen,black]
chempadkey%Res-2bRE-Hexose%C6H10O6%residue Hexose (2bRE)%[lawngreen,black]
chempadkey%Res-3b-Hexose%C6H9O5%residue Hexose (3b)%[lawngreen,black]
chempadkey%Res-4b-Hexose%C6H8O5%residue Hexose (4b)%[lawngreen,black]

chempadkey%Res-1b-Fucose%C6H11O4%residue Fucose%[lawngreen,black]
chempadkey%Res-1b-Xylose%C5H8O4%%residue Xylose%[lawngreen,black]


chempadgroup%Hexosamines && Sialic%[violet]

chempadkey%Res-1bRE-NAcHexosamine%C8H14O6N%residue NAcHexosamine (1bRE)%[orange,black]
chempadkey%Res-1bRE-NAcHexosaminol%C8H16O6N%residue NAcHexosaminol (1bRE-ol)%[orange,black]
chempadkey%Res-1bNRE-NAcHexosamine%C8H14O5N%residue NAcHexosamine (1bNRE)%[orange,black]
chempadkey%Res-2bRE-NAcHexosamine%C8H13O6N%residue NAcHexosamine (2bRE)%[orange,black]
chempadkey%Res-2bRE-NAcHexosaminol%C8H15O6N%residue NAcHexosaminol (2bRE-ol)%[orange,black]
chempadkey%Res-2b-NAcHexosamine%C8H13O5N%residue NAcHexosamine (2b)%[orange,black]
chempadkey%Res-3b-NAcHexosamine%C8H12O5N%residue NAcHexosamine (3b)%[orange,black]

chempadkey%Res-1b-Neu5Ac%C11H18O8N%residue Neu5Ac%[orange,black]
chempadkey%Res-1b-Neu5Gc%C11H18O9N%residue Neu5Gc%[orange,black]


chempadgroup%Permethylated Hexoses && Fucose && Xylose%[red]

chempadkey%Res-permeth-1bRE-Hexose%C10H19O6%residue permeth Hexose (1bRE)%[lawngreen,black]
chempadkey%Res-permeth-1bRE-Hexalditol%C11H23O6%residue permeth Hexalditol (1bRE-ol)%[lawngreen,black]
chempadkey%Res-permeth-1bNRE-Hexose%C10H19O5%residue permeth Hexose (1bNRE)%[lawngreen,black]
chempadkey%Res-permeth-2b-Hexose%C9H16O5%residue permeth Hexose (2b)%[lawngreen,black]
chempadkey%Res-permeth-2bRE-Hexose%C9H16O6%residue permeth Hexose (2bRE)%[lawngreen,black]
chempadkey%Res-permeth-3b-Hexose%C8H13O5%residue permeth Hexose (3b)%[lawngreen,black]
chempadkey%Res-permeth-4b-Hexose%C7H10O5%residue permeth Hexose (4b)%[lawngreen,black]

chempadkey%Res-permeth-1b-Fucose%C9H17O4%residue permeth Fucose%[lawngreen,black]
chempadkey%Res-permeth-1b-Xylose%C7H12O4%residue permeth Xylose%[lawngreen,black]


chempadgroup%Permethylated Hexosamines && Sialic%[red]

chempadkey%Res-permeth-1bRE-NAcHexosamine%C12H22O6N%residue permeth NAcHexosamine (1bRE)%[orange,black]
chempadkey%Res-permeth-1bRE-NAcHexosaminol%C13H26O6N%residue permeth NAcHexosaminol (1bRE-ol)%[orange,black]
chempadkey%Res-permeth-1bNRE-NAcHexosamine%C12H22O5N%residue permeth NAcHexosamine (1bNRE)%[orange,black]
chempadkey%Res-permeth-2bRE-NAcHexosamine%C11H19O6N%residue permeth NAcHexosamine (2bRE)%[orange,black]
chempadkey%Res-permeth-2bRE-NAcHexosaminol%C12H23O6N%residue permeth NAcHexosaminol (2bRE-ol)%[orange,black]
chempadkey%Res-permeth-2b-NAcHexosamine%C11H19O5N%residue permeth NAcHexosamine (2b)%[orange,black]
chempadkey%Res-permeth-3b-NAcHexosamine%C10H16O5N%residue permeth NAcHexosamine (3b)%[orange,black]
chempadkey%Res-permeth-3bRE-NAcHexosaminol%C11H20O6N%residue permeth NAcHexosaminol (3bRE-ol)%[orange,black]

chempadkey%Res-permeth-1b-Neu5Ac%C17H30O8N%residue permeth Neu5Ac%[orange,black]
chempadkey%Res-permeth-1b-Neu5Gc%C18H32O9N%residue permeth Neu5Gc%[orange,black]
