/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Local includes
#include "FragOptions.hpp"
#include <libXpertMass/PolChemDef.hpp>
#include <libXpertMass/PolChemDefEntity.hpp>

namespace MsXpS
{

namespace massxpert
{


FragOptions::FragOptions(libXpertMass::PolChemDefCstSPtr polChemDefCstSPtr,
                         QString name,
                         QString formula,
                         libXpertMass::FragEnd fragEnd,
                         const QString &comment,
                         bool sequenceEmbedded)
  : libXpertMass::FragSpec(polChemDefCstSPtr, name, formula, fragEnd, comment),
    m_sequenceEmbedded(sequenceEmbedded)
{
  // Have to be 1 and not 0, otherwise the setting functions ensuring
  // that m_startIonizeLevel>=m_endIonizeLevel will bug.
  m_startIonizeLevel = 1;
  m_endIonizeLevel   = 1;
}


FragOptions::FragOptions(const libXpertMass::FragSpec &fragSpec,
                         int startIndex,
                         int endIndex,
                         bool sequenceEmbedded)
  : libXpertMass::FragSpec(fragSpec),
    m_startIndex(startIndex),
    m_endIndex(endIndex),
    m_sequenceEmbedded(sequenceEmbedded)
{
  // Have to be 1 and not 0, otherwise the setting functions ensuring
  // that m_startIonizeLevel>=m_endIonizeLevel will bug.
  m_startIonizeLevel = 1;
  m_endIonizeLevel   = 1;
}


FragOptions::FragOptions(const FragOptions &other)
  : libXpertMass::FragSpec(other),
    m_startIndex(other.m_startIndex),
    m_endIndex(other.m_endIndex),
    m_startIonizeLevel(other.m_startIonizeLevel),
    m_endIonizeLevel(other.m_endIonizeLevel),
    m_sequenceEmbedded(other.m_sequenceEmbedded)
{
  for(int iter = 0; iter < other.m_formulaList.size(); ++iter)
    {
      libXpertMass::Formula *formula =
        new libXpertMass::Formula(*other.m_formulaList.at(iter));
      m_formulaList.append(formula);
    }
}


FragOptions::~FragOptions()
{
  // There might be some allocated libXpertMass::Formula instances in the
  // m_formulaList. Free them.

  while(!m_formulaList.isEmpty())
    delete m_formulaList.takeFirst();
}


FragOptions &
FragOptions::operator=(const FragOptions &other)
{
  if(&other == this)
    return *this;

  libXpertMass::FragSpec::operator=(other);

  m_startIndex = other.m_startIndex;
  m_endIndex   = other.m_endIndex;

  m_startIonizeLevel = other.m_startIonizeLevel;
  m_endIonizeLevel   = other.m_endIonizeLevel;

  m_sequenceEmbedded = other.m_sequenceEmbedded;

  for(int iter = 0; iter < other.m_formulaList.size(); ++iter)
    {
      libXpertMass::Formula *formula =
        new libXpertMass::Formula(*other.m_formulaList.at(iter));
      m_formulaList.append(formula);
    }

  return *this;
}


void
FragOptions::setStartIndex(int value)
{
  m_startIndex = value;
}


int
FragOptions::startIndex() const
{
  return m_startIndex;
}


void
FragOptions::setEndIndex(int value)
{
  m_endIndex = value;
}


int
FragOptions::endIndex() const
{
  return m_endIndex;
}


void
FragOptions::setStartIonizeLevel(int value)
{
  int local = (value < 0) ? abs(value) : value;

  if(local <= m_endIonizeLevel)
    {
      m_startIonizeLevel = local;
    }
  else
    {
      m_startIonizeLevel = m_endIonizeLevel;
      m_endIonizeLevel   = local;
    }
}


int
FragOptions::startIonizeLevel() const
{
  return m_startIonizeLevel;
}


void
FragOptions::setEndIonizeLevel(int value)
{
  int local = (value < 0) ? abs(value) : value;

  if(local > m_startIonizeLevel)
    {
      m_endIonizeLevel = local;
    }
  else
    {
      m_startIonizeLevel = m_endIonizeLevel;
      m_endIonizeLevel   = local;
    }
}


int
FragOptions::endIonizeLevel() const
{
  return m_endIonizeLevel;
}

bool
FragOptions::addFormula(const libXpertMass::Formula &formula)
{
  // First check that the formula is not already in the list.

  for(int iter = 0, size = m_formulaList.size(); iter < size; ++iter)
    {
      if(*(m_formulaList.at(iter)) == formula)
        return true;
    }

  // At this point we can say that the formula is OK and can be
  // added.

  libXpertMass::Formula *newFormula = new libXpertMass::Formula(formula);

  // At this point, we should check if the formula is valid.

  libXpertMass::IsotopicDataCstSPtr isotopic_data_csp =
    mcsp_polChemDef->getIsotopicDataCstSPtr();

  if(!newFormula->validate(isotopic_data_csp))
    {
      qDebug() << "Formula" << newFormula->toString() << "failed to validate.";

      delete newFormula;

      return false;
    }

  m_formulaList.append(newFormula);

  return true;
}

bool
FragOptions::addFormula(const QString &text)
{
  // With the string make a true libXpertMass::Formula instance.

  libXpertMass::Formula formula = libXpertMass::Formula(text);

  return addFormula(formula);
}


const QList<libXpertMass::Formula *> &
FragOptions::formulaList()
{
  return m_formulaList;
}

void
FragOptions::setSequenceEmbedded(bool value)
{
  m_sequenceEmbedded = value;
}


bool
FragOptions::isSequenceEmbedded() const
{
  return m_sequenceEmbedded;
}


QString
FragOptions::toString() const
{
  QString text = "Fragoptions: ";

  text += QString("indices [%1-%2] -- ionization levels [%3-%4].\n")
            .arg(m_startIndex)
            .arg(m_endIndex)
            .arg(m_startIonizeLevel)
            .arg(m_endIonizeLevel);

  if(m_formulaList.size())
    {
      text += "Formulas:\n";

      QList<libXpertMass::Formula *>::const_iterator iter = m_formulaList.cbegin();
      QList<libXpertMass::Formula *>::const_iterator iter_end = m_formulaList.cend();

      while(iter != iter_end)
        {
          text += QString("%1 - ").arg((*iter)->toString());
          ++iter;
        }

      text += "\n";
    }

  return text;
}

} // namespace massxpert

} // namespace MsXpS
