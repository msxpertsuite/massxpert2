/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// libmass includes
#include <libXpertMass/Polymer.hpp>
#include <libXpertMass/Oligomer.hpp>


/////////////////////// Local includes
#include "OligomerList.hpp"


namespace MsXpS
{

namespace massxpert
{


OligomerList::OligomerList(QString name,
                           libXpertMass::Polymer *polymer,
                           libXpertMass::MassType massType)
  : m_name(name), mp_polymer(polymer), m_massType(massType)
{
}


OligomerList::OligomerList(const OligomerList *other)
  : QList<libXpertMass::OligomerSPtr>(*other),
    m_name(other->m_name),
    m_comment(other->m_comment),
    mp_polymer(other->mp_polymer),
    m_massType(other->m_massType)
{
  // And now copy all the items from other in here:
  for(int iter = 0; iter < other->size(); ++iter)
    append(std::make_shared<libXpertMass::Oligomer>(*other->at(iter)));
}


OligomerList::~OligomerList()
{
  clear();
}


//! Sets the name.
/*!  \param name new name.
 */
void
OligomerList::setName(QString name)
{
  if(!name.isEmpty())
    m_name = name;
}


QString
OligomerList::name()
{
  return m_name;
}


//! Sets the comment.
/*!  \param comment new comment.
 */
void
OligomerList::setComment(QString comment)
{
  if(!comment.isEmpty())
    m_comment = comment;
}


const QString &
OligomerList::comment() const
{
  return m_comment;
}


const libXpertMass::Polymer *
OligomerList::polymer() const
{
  return mp_polymer;
}


void
OligomerList::setMassType(libXpertMass::MassType massType)
{
  m_massType = massType;
}


libXpertMass::MassType
OligomerList::massType() const
{
  return m_massType;
}


libXpertMass::OligomerSPtr
OligomerList::findOligomerEncompassing(int index, int *listIndex)
{
  // Is there any oligomer that emcompasses the index ? If so return
  // its pointer. Start searching in the list at index *listIndex if
  // parameter non-0.

  int iter = 0;

  if(listIndex)
    {
      if(*listIndex >= size())
        return 0;

      if(*listIndex < 0)
        return 0;

      iter = *listIndex;
    }

  for(; iter < size(); ++iter)
    {
      libXpertMass::OligomerSPtr oligomer_sp = at(iter);

      if(oligomer_sp->encompasses(index))
        {
          if(listIndex)
            *listIndex = iter;

          return oligomer_sp;
        }
    }

  return 0;
}


libXpertMass::OligomerSPtr
OligomerList::findOligomerEncompassing(const libXpertMass::Monomer *monomer,
                                       int *listIndex)
{
  // Is there any oligomer that emcompasses the monomer ? If so return
  // its pointer. Start searching in the list at index *listIndex if
  // parameter non-0.

  Q_ASSERT(monomer);

  int iter = 0;

  if(listIndex)
    {
      if(*listIndex >= size())
        return 0;

      if(*listIndex < 0)
        return 0;

      iter = *listIndex;
    }

  for(; iter < size(); ++iter)
    {
      libXpertMass::OligomerSPtr oligomer_sp = at(iter);

      if(oligomer_sp->encompasses(monomer))
        {
          if(listIndex)
            *listIndex = iter;

          return oligomer_sp;
        }
    }

  return 0;
}


QString *
OligomerList::makeMassText(libXpertMass::MassType massType)
{
  QString *text = new QString();

  libXpertMass::MassType localMassType = libXpertMass::MassType::MASS_NONE;

  if(massType == libXpertMass::MassType::MASS_BOTH)
    {
      delete text;
      return 0;
    }

  if(massType & libXpertMass::MassType::MASS_NONE)
    localMassType = m_massType;
  else
    localMassType = massType;

  for(int iter = 0; iter < size(); ++iter)
    {
      double value = 0;

      if(localMassType & libXpertMass::MassType::MASS_MONO)
        value = at(iter)->mono();
      else
        value = at(iter)->avg();

      QString mass;
      mass.setNum(value, 'f', libXpertMass::OLIGOMER_DEC_PLACES);

      text->append(mass + "\n");
    }

  return text;
}


bool
OligomerList::lessThanMono(const libXpertMass::OligomerSPtr o1,
                           const libXpertMass::OligomerSPtr o2)
{
  qDebug() << "o1 is:" << o1.get() << "o2 is:" << o2.get();

  return true;
}


bool
OligomerList::lessThanAvg(const libXpertMass::OligomerSPtr o1,
                          const libXpertMass::OligomerSPtr o2)
{
  qDebug() << "o1 is:" << o1.get() << "o2 is:" << o2.get();

  return true;
}


void
OligomerList::sortAscending()
{
  // We only can sort if this instance knows which mass type it should
  // handle, and that cannot be anything other than MassType::MASS_MONO
  // or MASS_AVG.

  if(m_massType == libXpertMass::MassType::MASS_BOTH ||
     m_massType & libXpertMass::MassType::MASS_NONE)
    {
      qDebug() << "Could not sort the oligomer list: "
                  "m_massType is not correct";

      return;
    }

  if(m_massType == libXpertMass::MassType::MASS_MONO)
    std::sort(begin(), end(), OligomerList::lessThanMono);
  else
    std::sort(begin(), end(), OligomerList::lessThanAvg);
}

} // namespace massxpert

} // namespace MsXpS
