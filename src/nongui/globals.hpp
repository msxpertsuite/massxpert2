/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


////////////////////////////// Qt includes
#include <QString>
#include <QList>
#include <QTextStream>
#include <QRegularExpression>


/*
 * Bitwise stuff (from StackOverflow)

 * It is sometimes worth using an enum to name the bits:

 * enum ThingFlags = {
 * ThingMask  = 0x0000,
 * ThingFlag0 = 1 << 0,
 * ThingFlag1 = 1 << 1,
 * ThingError = 1 << 8,
 * }

 * Then use the names later on. I.e. write

 * thingstate |= ThingFlag1;
 * thingstate &= ~ThingFlag0;
 * if (thing & ThingError) {...}

 * to set, clear and test. This way you hide the magic numbers from the rest of
 * your code.
 */

namespace MsXpS
{

namespace massxpert
{


  //! Type of user that makes use of resources.
  /*!
   * When looking for resources, this enum variables allow defining if the
   * resources are to be searched in system directories or in the user $HOME
   * directory.
   */
  enum UserType
  {
    USER_TYPE_USER   = 0, //!< User is a logon user
    USER_TYPE_SYSTEM = 1  //!< User is the system
  };

  extern QChar gConfigKeyStringPrefixDelimiter;


enum class MassSpectrumSynthesisActions
{
  LOAD_ISOTOPIC_DATA = 0,
  CONFIGURE_MASS_PEAK_SHAPER,
  SYNTHESIZE_MASS_SPECTRA,
  COPY_MASS_SPECTRUM_TO_CLIPBOARD,
  SAVE_MASS_SPECTRUM_TO_FILE
};


} // namespace massxpert

} // namespace MsXpS
