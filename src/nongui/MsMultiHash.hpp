/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// Qt includes
#include <QMultiHash>
#include <QList>

/////////////////////// Local includes
#include "MassSpectrum.hpp"

namespace MsXpS
{

	namespace massxpert
	{



//! The MsMultiHash class provides a multi-hash relating double values with
//! MassSpectrum instance pointers.
/*!

This QMultiHash-based class is used when a set of Massspectrum instances needs
to be related to double values, like retention times or drift times. This is
why the methods allow for getting ranged keys or values, such like when asking
for all the mass spectra corresponding to a retention time contained in a
given time range.

 */
class MsMultiHash : public QMultiHash<double, MassSpectrum *>
{
  public:
  MsMultiHash();

  ~MsMultiHash();

  int rangedKeys(QList<double> &keyList, double start, double end) const;
  using QMultiHash::values;
  int values(double key, QList<MassSpectrum *> &massSpectra) const;
  int rangedValues(QList<MassSpectrum *> &massSpectra,
                   double start,
                   double end) const;
};


} // namespace massxpert

} // namespace MsXpS
