/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef OLIGOMER_LIST_HPP
#define OLIGOMER_LIST_HPP


/////////////////////// Qt includes


/////////////////////// Local includes
#include <libXpertMass/globals.hpp>
#include <libXpertMass/Oligomer.hpp>


namespace MsXpS
{

	namespace massxpert
	{



class OligomerList : public QObject, public QList<libXpertMass::OligomerSPtr>
{
  Q_OBJECT

  private:
  QString m_name;
  QString m_comment;
  libXpertMass::Polymer *mp_polymer;
  libXpertMass::MassType m_massType;

  public:
  OligomerList(QString         = QString(),
               libXpertMass::Polymer *       = 0,
               libXpertMass::MassType = libXpertMass::MassType::MASS_BOTH);
  OligomerList(const OligomerList *);

  virtual ~OligomerList();

  void setName(QString);
  QString name();

  void setComment(QString);
  const QString &comment() const;

  const libXpertMass::Polymer *polymer() const;

  void setMassType(libXpertMass::MassType);
  libXpertMass::MassType massType() const;

  libXpertMass::OligomerSPtr findOligomerEncompassing(int index, int * = 0);
  libXpertMass::OligomerSPtr findOligomerEncompassing(const libXpertMass::Monomer *, int * = 0);

  // In the methods below, MassType is by default MASS_NONE,
  // which means that the value of the variable is to be read in the
  // member m_massType.

  QString *makeMassText(libXpertMass::MassType = libXpertMass::MassType::MASS_NONE);

  static bool lessThanMono(const libXpertMass::OligomerSPtr, const libXpertMass::OligomerSPtr);
  static bool lessThanAvg(const libXpertMass::OligomerSPtr, const libXpertMass::OligomerSPtr);

  void sortAscending();
  void sortDescending();

  void applyMass(double, libXpertMass::MassType = libXpertMass::MassType::MASS_NONE);
  void removeLessThan(double, libXpertMass::MassType = libXpertMass::MassType::MASS_NONE);
};

} // namespace massxpert

} // namespace MsXpS


#endif // OLIGOMER_LIST_HPP
