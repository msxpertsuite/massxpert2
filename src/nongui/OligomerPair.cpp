/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Local includes
#include <libXpertMass/globals.hpp>
#include "OligomerPair.hpp"
#include <libXpertMass/Oligomer.hpp>


namespace MsXpS
{

namespace massxpert
{


OligomerPair::OligomerPair(const libXpertMass::OligomerSPtr oligomer1_sp,
                           const libXpertMass::OligomerSPtr oligomer2_sp,
                           libXpertMass::MassType massType,
                           double error,
                           bool isMatching,
                           const QString &name)
  : msp_oligomer1(oligomer1_sp),
    msp_oligomer2(oligomer2_sp),
    m_massType(massType),
    m_error(error),
    m_isMatching(isMatching),
    m_name(name)
{
  Q_ASSERT(msp_oligomer1);
  Q_ASSERT(msp_oligomer2);
}


OligomerPair::OligomerPair(const OligomerPair &other)
  : libXpertMass::PropListHolder(other),
    msp_oligomer1(other.msp_oligomer1),
    msp_oligomer2(other.msp_oligomer2),
    m_massType(other.m_massType),
    m_error(other.m_error),
    m_isMatching(other.m_isMatching),
    m_name(other.m_name)
{
  Q_ASSERT(msp_oligomer1);
  Q_ASSERT(msp_oligomer2);
}


OligomerPair::~OligomerPair()
{
}


QString
OligomerPair::name()
{
  return m_name;
}


const libXpertMass::OligomerSPtr
OligomerPair::oligomer1() const
{
  return msp_oligomer1;
}


const libXpertMass::OligomerSPtr
OligomerPair::oligomer2() const
{
  return msp_oligomer2;
}


void
OligomerPair::setError(double error)
{
  m_error = error;
}


libXpertMass::MassType
OligomerPair::massType() const
{
  return m_massType;
}


void
OligomerPair::setMassType(libXpertMass::MassType massType)
{
  m_massType = massType;
}


double
OligomerPair::error() const
{
  return m_error;
}


void
OligomerPair::setMatching(bool isMatching)
{
  m_isMatching = isMatching;
}


bool
OligomerPair::isMatching() const
{
  return m_isMatching;
}


double
OligomerPair::mass1()
{
  return msp_oligomer1->mass(m_massType);
}


int
OligomerPair::charge1()
{
  return msp_oligomer1->charge();
}


double
OligomerPair::mass2()
{
  return msp_oligomer2->mass(m_massType);
}


int
OligomerPair::charge2()
{
  return msp_oligomer2->charge();
}

} // namespace massxpert

} // namespace MsXpS
