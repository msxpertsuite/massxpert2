/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef DECIMAL_PLACES_OPTIONS_DLG_HPP
#define DECIMAL_PLACES_OPTIONS_DLG_HPP


/////////////////////// Qt includes
#include <QMainWindow>


/////////////////////// Local includes
#include "ui_DecimalPlacesOptionsDlg.h"
#include "SequenceEditorWnd.hpp"


namespace MsXpS
{

	namespace massxpert
	{



class DecimalPlacesOptionsDlg : public QDialog
{
  Q_OBJECT

  private:
  Ui::DecimalPlacesOptionsDlg m_ui;

  QString m_configSettingsFilePath;

  SequenceEditorWnd *mp_editorWnd;

  void closeEvent(QCloseEvent *event);

  bool fetchValidateInputData();

  public:
  DecimalPlacesOptionsDlg(QWidget *, const QString &configSettingsFilePath);

  ~DecimalPlacesOptionsDlg();

  SequenceEditorWnd *editorWnd();

  public slots:
  void validate();

  signals:
  void updateWholeSequenceMasses();
  void updateSelectedSequenceMasses();
};

} // namespace massxpert

} // namespace MsXpS


#endif // DECIMAL_PLACES_OPTIONS_DLG_HPP
