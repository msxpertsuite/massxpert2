/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef MZ_CALCULATION_DLG_HPP
#define MZ_CALCULATION_DLG_HPP


/////////////////////// Qt includes
#include <QMainWindow>
#include <QSortFilterProxyModel>
#include <libXpertMass/PolChemDefEntity.hpp>


/////////////////////// libmass includes
#include <libXpertMass/PolChemDef.hpp>
#include <libXpertMass/Ionizable.hpp>

/////////////////////// Local includes
#include "ui_MzCalculationDlg.h"
#include "MzCalculationTreeViewSortProxyModel.hpp"
#include "../nongui/OligomerList.hpp"

namespace MsXpS
{

namespace massxpert
{


class MzCalculationTreeViewModel;
class MzCalculationTreeViewSortProxyModel;
class PolChemDef;

class MzCalculationDlg : public QDialog
{
  Q_OBJECT

  public:
  MzCalculationDlg(QWidget *,
                   const QString &configSettingsFilePath,
                   const libXpertMass::PolChemDefCstSPtr,
                   const QString &applicationName,
                   const QString &description,
                   const libXpertMass::IonizeRule *,
                   double = 0,
                   double = 0);

  ~MzCalculationDlg();

  void setupTreeView();

  bool getSrcIonizeRuleData(libXpertMass::IonizeRule *);
  bool getDestIonizeRuleData(libXpertMass::IonizeRule *);

  libXpertMass::Ponderable getSourcePonderable();

  void freeIonizableList();
  void emptyIonizableList();

  // The results-exporting functions. ////////////////////////////////
  void prepareResultsTxtString();
  bool exportResultsClipboard();
  bool exportResultsFile();
  bool selectResultsFile();
  bool calculateSpectrum();
  //////////////////////////////////// The results-exporting functions.

  public slots:
  void calculate();
  void formulaCheckBoxToggled(bool);
  void exportResults(int);

  private:
  Ui::MzCalculationDlg m_ui;

  // The results-exporting strings. ////////////////////////////////
  QString *mpa_resultsString;
  QString m_resultsFilePath;
  //////////////////////////////////// The results-exporting strings.

  QString m_configSettingsFilePath;

  libXpertMass::PolChemDefCstSPtr mcsp_polChemDef;

  OligomerList m_oligomerList;

  libXpertMass::Formula m_formula;

  libXpertMass::IonizeRule m_ionizeRule;

  QList<libXpertMass::Ionizable *> m_ionizableList;

  MzCalculationTreeViewModel *mpa_mzTreeViewModel;
  MzCalculationTreeViewSortProxyModel *mpa_mzProxyModel;

  void closeEvent(QCloseEvent *event);
};

} // namespace massxpert

} // namespace MsXpS


#endif // MZ_CALCULATION_DLG_HPP
