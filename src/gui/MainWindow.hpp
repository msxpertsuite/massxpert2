/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QMainWindow>
#include <QDir>
#include <QStringList>


/////////////////////// local includes
#include "AboutDlg.hpp"
#include "../nongui/UserSpec.hpp"
#include "../nongui/ConfigSettings.hpp"
#include <libXpertMass/PolChemDef.hpp>
#include <libXpertMass/PolChemDefSpec.hpp>


class QAction;
class QMenu;
class QTextEdit;


namespace MsXpS
{

	namespace massxpert
	{



class SequenceEditorWnd;


class MainWindow : public QMainWindow
{
  Q_OBJECT

  public:
  MainWindow(const QString &moduleName);
  ~MainWindow();

  QString configSettingsFilePath() const;
  const UserSpec &userSpec() const;
  const ConfigSettings *configSettings() const;

  bool setConfigSettingsManually();

  void initializeDecimalPlacesOptions();

  void setLastFocusedSeqEdWnd(SequenceEditorWnd *);

  QList<libXpertMass::PolChemDefSpec *> *polChemDefCatList();
  libXpertMass::PolChemDefSpec *polChemDefSpecName(const QString &);
  libXpertMass::PolChemDefSpec *polChemDefSpecFilePath(const QString &);

  void polChemDefCatStringList(QStringList &stringList);

  std::vector<libXpertMass::PolChemDefCstSPtr> polChemDefList();
  libXpertMass::PolChemDefCstSPtr polChemDefByName(const QString &name);

  bool isSequenceEditorWnd(SequenceEditorWnd *) const;

  public slots:
  void openPolChemDef();
  void newPolChemDef();

  void newCalculator();

  void openSequence(const QString &fileName = QString());
  void openSampleSequence();
  void newSequence();
  void delistSequenceEditorWnd(SequenceEditorWnd *wnd);

  void mzLab();

  void about();
	void showAboutDlg();

  void massListSorter();
  void seqTools();

  protected:
  void closeEvent(QCloseEvent *);

  private:
  QString m_moduleName;

  UserSpec m_userSpec;
  QString m_configSettingsFilePath;
  ConfigSettings *mpa_configSettings = nullptr;

  void readSettings();
  void writeSettings();

  // Last sequence editor window that got the focus.
  SequenceEditorWnd *mp_lastFocusedSeqEdWnd;

  // The QList of all the sequence editor windows.
  QList<SequenceEditorWnd *> m_sequenceEditorWndList;

  // List of all the polymer chemistry definition specifications that
  // were created during parsing of all the different catalogue files
  // on the system.
  QList<libXpertMass::PolChemDefSpec *> m_polChemDefCatList;

  // List of all the polymer chemistry definitions that are loaded in
  // memory and usable to load polymer sequences.
  std::vector<libXpertMass::PolChemDefCstSPtr> m_polChemDefList;

  QMenu *fileMenu;

  QMenu *xpertDefMenu;
  QMenu *xpertCalcMenu;
  QMenu *xpertEditMenu;
  QMenu *xpertMinerMenu;
  QMenu *toolsMenu;

  QMenu *helpMenu;

  QAction *openPolChemDefAct;
  QAction *newPolChemDefAct;

  QAction *newCalculatorAct;

  QAction *openSequenceAct;
  QAction *openSampleSequenceAct;
  QAction *newSequenceAct;

  QAction *mzLabAct;

  QAction *massListSorterAct;
  QAction *seqToolsAct;

  QAction *exitAct;
  QAction *configSettingsAct;

  QAction *aboutAct;
  QAction *aboutQtAct;

  void addToMenu(QObject *, const QStringList &, QMenu *, const char *);

  void createActions();
  void createMenus();
  void createStatusBar();

  void setupWindow();
  bool setupConfigSettings();
  bool initializeSystemConfig();
  bool initializeUserConfig();


};

} // namespace massxpert

} // namespace MsXpS


