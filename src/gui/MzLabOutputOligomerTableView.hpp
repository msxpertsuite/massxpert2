/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef MZ_LAB_OUTPUT_OLIGOMER_TABLE_VIEW_HPP
#define MZ_LAB_OUTPUT_OLIGOMER_TABLE_VIEW_HPP


/////////////////////// Qt includes
#include <QTableView>


/////////////////////// Local includes
#include "../nongui/OligomerPair.hpp"
#include "../nongui/OligomerList.hpp"

namespace MsXpS
{

	namespace massxpert
	{



class MzLabWnd;
class MzLabOutputOligomerTableViewModel;
class MzLabOutputOligomerTableViewDlg;


class MzLabOutputOligomerTableView : public QTableView
{
  Q_OBJECT

  private:
  MzLabOutputOligomerTableViewDlg *mp_parentDlg;
  MzLabWnd *mp_mzLabWnd;

  MzLabOutputOligomerTableViewModel *mp_sourceModel;

  // We do not own that oligomerPair list.
  QList<OligomerPairSPtr> *mp_oligomerPairList;


  protected:
  public:
  MzLabOutputOligomerTableView(QWidget *parent = 0);
  ~MzLabOutputOligomerTableView();

  void setSourceModel(MzLabOutputOligomerTableViewModel *);
  MzLabOutputOligomerTableViewModel *sourceModel();

  void setOligomerPairList(QList<OligomerPairSPtr> *);
  const QList<OligomerPairSPtr> *oligomerPairList();

  void setParentDlg(MzLabOutputOligomerTableViewDlg *);
  MzLabOutputOligomerTableViewDlg *parentDlg();

  void setMzLabWnd(MzLabWnd *);
  MzLabWnd *mzLabWnd();

  void provideEditorWindowFeedback(const libXpertMass::OligomerSPtr);

  QString *selectedDataAsPlainText();

  int duplicateSelectedOligomers(OligomerList *);

  public slots:
  void itemActivated(const QModelIndex &);
};

} // namespace massxpert

} // namespace MsXpS


#endif // MZ_LAB_OUTPUT_OLIGOMER_TABLE_VIEW_HPP
