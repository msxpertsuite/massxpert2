/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QGraphicsSvgItem>
#include <QSvgRenderer>


/////////////////////// Local includes
#include "ChemEntVignetteRenderer.hpp"
#include <libXpertMass/PolChemDefEntity.hpp>


namespace MsXpS {

namespace massxpert
{
	

class ChemEntVignette : public QGraphicsSvgItem
{
  Q_OBJECT

  private:
  QString m_name;

  // We do not own this!
  libXpertMass::PolChemDefEntity *mp_owner;

  // We do not own this!
  ChemEntVignetteRenderer *mp_renderer;

  public:
  ChemEntVignette(QGraphicsSvgItem *parent = 0);

  ChemEntVignette(const QString &fileName, QGraphicsSvgItem *parent = 0);

  ~ChemEntVignette();

  QString name();
  void setName(QString);

  libXpertMass::PolChemDefEntity *owner();
  void setOwner(libXpertMass::PolChemDefEntity *);

  void setSharedRenderer(ChemEntVignetteRenderer *);

  ChemEntVignetteRenderer *renderer();
};

} // namespace massxpert

} // namespace MsXpS


