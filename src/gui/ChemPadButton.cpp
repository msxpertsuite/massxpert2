/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QDebug>
#include <QMessageBox>
#include <QKeyEvent>


/////////////////////// Local includes
#include "ChemPadButton.hpp"
#include "CalculatorWnd.hpp"

namespace MsXpS
{

namespace massxpert
{


  ChemPadButton::ChemPadButton(const QString &text,
                               const QString &formula,
                               QWidget *calculator_wnd,
                               QWidget *parent,
                               const QColor &backgroundColor,
                               const QColor &foregroundColor)
    : QPushButton(text, parent)
  {
    Q_ASSERT(calculator_wnd);

    mp_calculatorWnd = static_cast<CalculatorWnd *>(calculator_wnd);
    m_formula        = formula;

    if(backgroundColor.isValid() && foregroundColor.isValid())
      {
        QString bgColorString = QString("rgb(%1,%2,%3)")
                                  .arg(backgroundColor.red())
                                  .arg(backgroundColor.green())
                                  .arg(backgroundColor.blue());

        QString fgColorString = QString("rgb(%1,%2,%3)")
                                  .arg(foregroundColor.red())
                                  .arg(foregroundColor.green())
                                  .arg(foregroundColor.blue());

        setStyleSheet(QString("QPushButton {background: %1 ; color: %2}")
                        .arg(bgColorString)
                        .arg(fgColorString));
      }

    connect(this, SIGNAL(clicked()), this, SLOT(clicked()));
  }


  ChemPadButton::~ChemPadButton()
  {
  }


  void
  ChemPadButton::clicked()
  {
    // We have to account for the formula corresponding to this button.

    return mp_calculatorWnd->apply(m_formula);
  }


} // namespace massxpert

} // namespace MsXpS
