/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef MONOMER_CODE_EVALUATOR_HPP
#define MONOMER_CODE_EVALUATOR_HPP


/////////////////////// Qt includes
#include <QLineEdit>


/////////////////////// Local includes
#include "SequenceEditorWnd.hpp"


namespace MsXpS
{

	namespace massxpert
	{



class Polymer;
class SequenceEditorWnd;


class MonomerCodeEvaluator : public QObject
{
  Q_OBJECT

  public:
  MonomerCodeEvaluator(libXpertMass::Polymer *           = 0,
                       SequenceEditorWnd * = 0,
                       QLineEdit *         = 0,
                       QLineEdit *         = 0);

  ~MonomerCodeEvaluator();

  void setSequenceEditorWnd(SequenceEditorWnd *);
  void setEdits(QLineEdit *, QLineEdit *);

  bool evaluateCode(const QString &);

  void escapeKey();
  bool newKey(QString);

  bool upperCaseChar(QChar);
  bool lowerCaseChar(QChar);

  int autoComplete(QString &);
  void emptyCompletionsList();
  bool reportCompletions();

  void reportError(QString &);

  void debugCompletionsPutStdErr();

  private:
  libXpertMass::Polymer *mp_polymer;
  SequenceEditorWnd *mp_editorWnd;

  QLineEdit *m_elabCodeLineEdit;
  QLineEdit *m_errorCodeLineEdit;

  QList<int> m_completionsList;


  QString m_evalCode;
  QString m_elabCode;

};

} // namespace massxpert

} // namespace MsXpS


#endif // MONOMER_CODE_EVALUATOR_HPP
